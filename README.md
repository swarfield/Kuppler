# Kuppler
Kuppler is a Crowd Source Dating App 

# Usage
As Kuppler uses Firebase for authentication, every devices' SHA key must be added to the `google-services.json` file. Get the key by running the following command in the the `KupplerApp` directory: 

```
keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore -list -v -storepass android
```

Then navigate the following path in the Firebase Consol for KupplerApp, `Settings -> General -> Add Fingerprint`. Then redownload the `google-services.json` file and replace it in the `KupplerApp/App' directory.

# Style Guide
Kuppler will strictly adhere to the Google Style Guide. Before all merges to master the adherence will be checked!
