package com.brianslittledarlings.onion.kuppler;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class LoadingInfoActivity extends SingleFragmentActivity {
    private final String BUNDLE = "bundle";

    @Override
    protected Fragment createFragment(){
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        return LoadingInfoFragment.newInstance(bundle);
    }
}
