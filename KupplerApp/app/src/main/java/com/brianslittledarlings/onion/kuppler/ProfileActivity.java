package com.brianslittledarlings.onion.kuppler;

import android.support.v4.app.Fragment;

import java.util.UUID;

public class ProfileActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment(){

        return ProfileFragment.newInstance((UUID) null);
    }


}
