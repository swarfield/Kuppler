package com.brianslittledarlings.onion.kuppler;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class MatchmakingChoiceFragment extends Fragment{
    //two button system for choice
    private Button mMatchmakingButton;
    private Button mProfileButton;
    private final String EXTRA_WINGMAN = "extra_wingman";


    public static MatchmakingChoiceFragment newInstance() {
        return new MatchmakingChoiceFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //start pull from Firebase, possibility to hang so async
        ProfileLab.get(getContext());
        MatchmakingLab.get(getContext());
    }

    /**
     * Creates the screen in which the user may choose between matchmaking and editing their profile
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_matchmaking_choice, container, false);

        //hookup buttons and text fields for profile finish
        mProfileButton = (Button) v.findViewById(R.id.make_or_edit_button);
        mProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Go edit profile
                Intent i = new Intent(getActivity(), ProfileEditActivity.class);
                startActivity(i);
            }
        });

        //hookup buttons and text fields for profile finish
        mMatchmakingButton = (Button) v.findViewById(R.id.just_mm_button);
        mMatchmakingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Starts process of Matchmaking
                Intent i = new Intent(getActivity(), ProfilePagerActivity.class);
                i.putExtra(EXTRA_WINGMAN,true);
                startActivity(i);
            }
        });
        return v;
    }
}
