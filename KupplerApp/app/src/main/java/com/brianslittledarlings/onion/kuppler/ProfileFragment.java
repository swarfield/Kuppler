package com.brianslittledarlings.onion.kuppler;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.UUID;

public class ProfileFragment extends Fragment{
    private static final String PROFILE_UUID = "profile_uuid";
    private static final String FIREBASE_UUID = "firebase_uuid";

    private Profile mProfile;

    private ImageView mProfPic;
    private TextView mBio, mName;

    public static ProfileFragment newInstance(@Nullable UUID id){
        //pass through uuid to self
        Bundle args = new Bundle();
        args.putSerializable(PROFILE_UUID, id);

        //make the profile frag with profile
        ProfileFragment frag = new ProfileFragment();
        frag.setArguments(args);

        return frag;
    }

    public static ProfileFragment newInstance(String firebaseId){
        //pass through uuid to self
        Bundle args = new Bundle();
        args.putSerializable(FIREBASE_UUID, firebaseId);

        //make the profile frag with profile
        ProfileFragment frag = new ProfileFragment();
        frag.setArguments(args);

        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //make it so the profile thinks it isn't visible to the user

        //get profile ID
        UUID proj_id = (UUID) getArguments().getSerializable(PROFILE_UUID);
        String firebaseId = (String) getArguments().getSerializable(FIREBASE_UUID);
        if (proj_id != null) {
            mProfile = ProfileLab.get(getActivity()).getProfile(proj_id);
        }
        else {
            try {
                mProfile = ProfileLab.get(getActivity()).getPotentialProfile(firebaseId);
            } catch (Exception e) {
                mProfile = MatchmakingLab.get(getActivity()).getProfile(firebaseId);
            }
        }
    }

     public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        mProfPic = (ImageView) v.findViewById(R.id.prof_pic);
        //set image bitmap from profile byte array
        mProfPic.setImageBitmap(MakeBitmap(mProfile.getmProfpic()));

        //set profile bio
        mBio = (TextView) v.findViewById(R.id.bio);
        mBio.setText(mProfile.getmBio());

        //Name from profile
        mName = (TextView) v.findViewById(R.id.name);
        mName.setText(mProfile.getmName());

        return v;
    }
    //decode image with builtins
    public Bitmap MakeBitmap(byte[] image){
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

}
