package com.brianslittledarlings.onion.kuppler;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class ProfileLab {
    //todo make firebase and fill
    public static final String TAG = "ProfileLab";
    public static final int MAX_PROFILES = 5;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabase;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private String mSeeking;
    private int mZip;
    private DataSnapshot mDataSnapshot;

    private static List<String> uids = new ArrayList<>();
    private static ProfileLab sProfileLab;
    
    private List<Profile> mProfiles;
    private List<Profile> mUserSpecificProfiles;
    private Set<String> mIncludedUIDsInSpecificProfiles;


    public static ProfileLab get(Context context) {
        if (sProfileLab == null) {
            sProfileLab = new ProfileLab(context);
        }
        return sProfileLab;
    }

    private ProfileLab(Context context) {
        mProfiles = new ArrayList<>();

        getDataSnapshotOnce();
    }

    public List<Profile> getProfiles() {
        return mProfiles;
    }

    //Assumes that is called after mDataSnapshot has been recieved


    public Profile getProfile(UUID id){
        for (Profile profile : mProfiles) {
            if (profile.getmId().equals(id)) {
                return profile;
            }
        }
        return null;
    }

    public Profile getProfile(String firebaseId){
        for (Profile profile : mProfiles) {
            if (profile.getmFireBaseUUID().equals(firebaseId)) {
                return profile;
            }
        }
        return null;
    }


    private void getDataSnapshotOnce() {
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabase = mFirebaseDatabase.getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        ValueEventListener getUsers = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mDataSnapshot = dataSnapshot;
                pullUUIDs();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        mDatabase.child("users").addListenerForSingleValueEvent(getUsers);
    }

    private void pullUUIDs() {
        for (DataSnapshot snap : mDataSnapshot.getChildren()) {
            try {
                // Checks if should appear and that it is not the logged in user
                if (Boolean.parseBoolean(snap.child("mVisablePull").getValue().toString())
                        && !snap.getKey().equals(mFirebaseUser.getUid())) {
                    uids.add(snap.getKey());
                    if (uids.size() == MAX_PROFILES) break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        createProfiles();
    }

    public void createProfiles() {
        for (String aUser : uids) {
            Profile profile = new Profile();
            profile.setmFireBaseUUID(aUser);

            try { //Try to get data on the current user
                profile.pullFireBase();
            } catch (Exception e){
                continue;
            }
            mProfiles.add(profile);
        }
    }

    /**
     * METHODS for when we have chosen a person to match for
     */

    public void generatePotentialMatches(String userMatchmakingForUid) {
        Log.d(TAG, "getPotentialMatches: start");
        mUserSpecificProfiles = new ArrayList<>();
        mIncludedUIDsInSpecificProfiles = new HashSet<>();
        mIncludedUIDsInSpecificProfiles.add(userMatchmakingForUid);
        getUserInfo(userMatchmakingForUid);

        reuseProfiles();
        findNewProfiles();
    }

    public List<Profile> getPotentialProfiles() {
        return mUserSpecificProfiles;
    }

    private void getUserInfo(String userMatchmakingForUid) {
        mSeeking = mDataSnapshot.child(userMatchmakingForUid).child("mSeeking").getValue().toString();
        mZip = Integer.parseInt(mDataSnapshot.child(userMatchmakingForUid).child("mZip").getValue().toString());

    }

    private void reuseProfiles() {
        for (Profile p : mProfiles) {
            // assumes that p is visible

            if (p.getmZip() == mZip && p.getmGender().equals(mSeeking)
                    && !mIncludedUIDsInSpecificProfiles.contains(p.getmFireBaseUUID())) {
                if (mUserSpecificProfiles.size() == MAX_PROFILES) break;
                mUserSpecificProfiles.add(p);
                mIncludedUIDsInSpecificProfiles.add(p.getmFireBaseUUID());
            }
        }
    }

    private void findNewProfiles() {
        Log.d(TAG, "findNewProfiles: Start");
        for (DataSnapshot snap : mDataSnapshot.getChildren()) {

            if (mUserSpecificProfiles.size() == MAX_PROFILES) break;
            if (Boolean.parseBoolean(snap.child("mVisablePull").getValue().toString())
                    && !mIncludedUIDsInSpecificProfiles.contains(snap.getKey())
                    && snap.child("mGender").getValue().toString().equals(mSeeking)
                    && Integer.parseInt(snap.child("mZip").getValue().toString()) == (mZip)) {
                //create the new profile
                Profile profile = new Profile();
                profile.setmFireBaseUUID(snap.getKey());
                try { //Try to get data on the current user
                    profile.pullFireBase();
                } catch (Exception e){
                    continue;
                }
                mUserSpecificProfiles.add(profile);
                mIncludedUIDsInSpecificProfiles.add(snap.getKey());
            }
        }
    }

    public Profile getPotentialProfile(String firebaseId){
        for (Profile profile : mUserSpecificProfiles) {
            if (profile.getmFireBaseUUID().equals(firebaseId)) {
                return profile;
            }
        }
        for (Profile profile : mProfiles) {
            if (profile.getmFireBaseUUID().equals(firebaseId)) {
                return profile;
            }
        }
        return null;
    }

}