package com.brianslittledarlings.onion.kuppler;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

public class ProfileEditFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener,
        AdapterView.OnItemSelectedListener{
    //intent tags
    public static final String TAG = "ProfileEditFragment";
    public static final String ANONYMOUS = "anonymous";
    private static final int REQUEST_CAMERA = 0;

    //views
    private Button mSignOut;
    private Button mBuildProfile;
    private String mUsername;
    private String mPhotoUrl;
    private ImageButton mProfPic;
    private Spinner mGenderField, mSeekingField;
    private GoogleApiClient mGoogleApiClient;

    //local vars
    private String mImageFilePath;
    private EditText mNameField, mBioField, mZipField, mAgeField;
    private TextView mVisablePullView;
    private ArrayAdapter<CharSequence> mAdapter;
    private String mUserName = "", mUserBio = "", mUserGender, mUserSeeking;
    private int mUserZip, mUserAge;
    private boolean mVisablePull;

    // Firebase instance variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabase;
    private FirebaseStorage mFirebaseStorage;
    private StorageReference mStorage;

    // Firebase Photo Refs
    private StorageReference mimagesFolderRef;
    private StorageReference mUserProfilePicRef;
    private String mPersonalFileExtension;


    //singleton
    public static ProfileEditFragment newInstance(){
        return new ProfileEditFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Configure Google API Client
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        // Setup spinner adapter
        mAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.genders, android.R.layout.simple_spinner_item);

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        // Checks if user is logged in
        // Lanches login activity
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(getActivity(), LoginActivity.class));
            return;
        } else {
            mUsername = mFirebaseUser.getDisplayName();
            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
            }
        }

        // Init Firebase Realtime Database
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabase = mFirebaseDatabase.getReference();
        profileDataStored();

        // Init Firebase photo store
        mFirebaseStorage = FirebaseStorage.getInstance();
        mStorage = mFirebaseStorage.getReference();

        // Create a child reference
        // imagesRef now points to "images"
        mimagesFolderRef = mStorage.child("ProfilePics");

        mPersonalFileExtension = mFirebaseUser.getUid() + ".jpeg";
        mUserProfilePicRef = mimagesFolderRef.child(mPersonalFileExtension);
    }

    @Override
    public void onResume(){
        super.onResume();
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(getActivity(), LoginActivity.class));
        } else {
            mUsername = mFirebaseUser.getDisplayName();
            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile_edit, container, false);

        //BLOCK TEXT VIEWS AND SET TO CORRESPONDING VARS
        //Text fields for user and profile
        mNameField = v.findViewById(R.id.Name);
        mNameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mUserName = editable.toString();
            }
        });

        mBioField = v.findViewById(R.id.bio);
        mBioField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mUserBio = editable.toString();
            }
        });

        mZipField = v.findViewById(R.id.Zip);
        mZipField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() != 0) {
                    mUserZip = Integer.parseInt(editable.toString());
                } else {
                    mUserZip = 0;
                }
            }
        });

        mAgeField = v.findViewById(R.id.Age);
        mAgeField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() != 0) {
                    mUserAge = Integer.parseInt(editable.toString());
                } else {
                    mUserAge = 0;
                }
            }
        });

        //https://developer.android.com/guide/topics/ui/controls/spinner
        mGenderField = v.findViewById(R.id.Gender);

        // Specify the layout to use when the list of choices appears
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mGenderField.setAdapter(mAdapter);
        mGenderField.setOnItemSelectedListener(this);

        mSeekingField = v.findViewById(R.id.Seeking);
        mSeekingField.setAdapter(mAdapter);
        mSeekingField.setOnItemSelectedListener(this);

        mBuildProfile = v.findViewById(R.id.save_finish);
        mBuildProfile.setOnClickListener(new View.OnClickListener() {
            //NOTE let camera app do this
            @Override
            public void onClick(View view) {
                finishProfileEdit();
            }
        });

        mVisablePullView = v.findViewById(R.id.VisablePull);

        //placeholder for picture

        mProfPic = (ImageButton) v.findViewById(R.id.prof_pic);
        mProfPic.setImageResource(R.drawable.ic_logo_grey);
        mProfPic.setOnClickListener(new View.OnClickListener() {
            //NOTE let camera app do this
            @Override
            public void onClick(View view) {
                //check for permission
                // if not, ask
                // go to camera app

                //request for picture permission
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                } else {
                    //I have permission, go outsource a picture
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            // Error occurred while creating the File
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri photoURI = FileProvider.getUriForFile(getContext(),
                                    "com.example.android.fileprovider",
                                    photoFile);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                            }
                        }
                    }
                }
            }
        });

        mSignOut = (Button) v.findViewById(R.id.sign_out);
        mSignOut.setOnClickListener(new View.OnClickListener() {
            //NOTE let camera app do this
            @Override
            public void onClick(View view) {
                finishProfileEdit();
                mFirebaseAuth.signOut();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);

                mUsername = ANONYMOUS;
                Log.d(TAG, "Logged out");
                //update the current user
                mFirebaseUser = mFirebaseAuth.getCurrentUser();

                //clear the activity stack
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        return v;
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
        switch (parent.getId()) {
            case R.id.Gender:
                mUserGender = parent.getItemAtPosition(pos).toString();
                break;
            case R.id.Seeking:
                mUserSeeking = parent.getItemAtPosition(pos).toString();
                break;
        }
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    public void profileDataStored() {
        mDatabase.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //String bio = dataSnapshot.child("mBio").getValue(String.class);
                Log.d(TAG, "onDataChange: " + mFirebaseUser);
                if (dataSnapshot.hasChild(mFirebaseUser.getUid()) && dataSnapshot.child(mFirebaseUser.getUid()).hasChildren()) {
                    //populate the fields

                    mNameField.setText(dataSnapshot.child(mFirebaseUser.getUid()).child("mName").getValue().toString());
                    mBioField.setText(dataSnapshot.child(mFirebaseUser.getUid()).child("mBio").getValue().toString());
                    mZipField.setText(dataSnapshot.child(mFirebaseUser.getUid()).child("mZip").getValue().toString());
                    mAgeField.setText(dataSnapshot.child(mFirebaseUser.getUid()).child("mAge").getValue().toString());


                    String userGender = dataSnapshot.child(mFirebaseUser.getUid()).child("mGender").getValue().toString();
                    int spinnerPosition = mAdapter.getPosition(userGender);
                    mGenderField.setSelection(spinnerPosition);

                    String userSeeking = dataSnapshot.child(mFirebaseUser.getUid()).child("mSeeking").getValue().toString();
                    spinnerPosition = mAdapter.getPosition(userSeeking);
                    mSeekingField.setSelection(spinnerPosition);


                    mVisablePull = Boolean.parseBoolean(
                            dataSnapshot.child(mFirebaseUser.getUid()).child("mVisablePull").getValue().toString());
                    mVisablePullView.setText("Profile is in the matchmaking pool: " + mVisablePull);

                    StorageReference profPic = mStorage.child("ProfilePics/" + mFirebaseUser.getUid() + ".jpeg");

                    try { //This gets files from firebase with a max of two megabytes

                        final long HALF_MEGABYTE = 1024 * 512;
                        profPic.getBytes(HALF_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                            @Override
                            public void onSuccess(byte[] bytes) {
                                // Data for "images/island.jpg" is returns, use this as needed
                                mImageFilePath = "ProfilePics/" + mFirebaseUser.getUid() + ".jpeg";
                                mProfPic.setImageBitmap(MakeBitmap(bytes));
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle any errors
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void finishProfileEdit() {
        if (checkProfileIsFilledOut(mUserName, mUserBio, mUserZip, mUserAge)) {
            if (mUserAge < 18){
                Toast.makeText(getContext(), "Users must be at least 18 years old",Toast.LENGTH_SHORT).show();
                return;
            }
            if (mUserZip < 10000){
                Toast.makeText(getContext(), "Please enter a valid zip code",Toast.LENGTH_SHORT).show();
                return;
            }
            writeNewUser(mUserName, mUserBio, mUserZip, mFirebaseUser.getUid(), mUserAge, mUserGender, mUserSeeking);
            uploadPhoto();
            Intent i = new Intent(getActivity(), ProfilePagerActivity.class);
            //prevents the stack from becoming a huge pile of pagers.
            i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(i);
        } else {
            Toast.makeText(getContext(), "Please Complete User Profile.", Toast.LENGTH_SHORT).show();
        }
    }

    private void writeNewUser(String userName, String userBio, int userZip, String userFirebaseUUID,
                              int userAge, String userGender, String mUserSeeking) {
        //put in maps
        Profile user = new Profile(userName, userBio, userZip, userFirebaseUUID, userAge, userGender, mUserSeeking);
        user.setmVisablePull(mVisablePull);
        mDatabase.child("users").child(userFirebaseUUID).setValue(user);
    }

    public boolean checkProfileIsFilledOut(String userName, String userBio, int userZip, int userAge) {
        return !(userName.length() == 0 || userBio.length() == 0 ||
                userZip == 0 || userAge == 0 || mImageFilePath == null);
    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        //Interpret camera sending picture
        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
            //change picture button to image
            mProfPic.setImageURI(Uri.fromFile(new File(mImageFilePath)));
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Toast.makeText(getContext(), "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mImageFilePath = image.getAbsolutePath();
        return image;
    }

    private void uploadPhoto() {
        // This catches the case when profile pic already exists
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(mImageFilePath);
            while (bitmap.getHeight() >= 4096 || bitmap.getWidth() >= 4096) {
                bitmap = Bitmap.createScaledBitmap(bitmap,(int)(bitmap.getWidth()*0.8), (int)(bitmap.getHeight()*0.8), true);
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
            byte[] data = baos.toByteArray();
            UploadTask uploadTask = mUserProfilePicRef.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    exception.printStackTrace();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Bitmap MakeBitmap(byte[] image){
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }
}
