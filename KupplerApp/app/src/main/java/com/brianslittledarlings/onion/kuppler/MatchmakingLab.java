package com.brianslittledarlings.onion.kuppler;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MatchmakingLab {
    //Tags
    public static final String TAG = "MatchmakingLab";
    public static final int MAX_PROFILES = 5;

    //local variables
    private static List<String> uids = new ArrayList<>();
    private static MatchmakingLab sMatchmakingLab;
    private List<Profile> mProfiles;
    private List<String> matches;

    //Firebase Variables
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabase;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DataSnapshot mDataSnapshot;

    public static MatchmakingLab get(Context context) {
        if (sMatchmakingLab == null) {
            sMatchmakingLab = new MatchmakingLab(context);
        }
        return sMatchmakingLab;
    }

    public List<Profile> getProfiles() {
        return mProfiles;
    }

    public Profile getProfile(String firebaseId) {
        for (Profile profile : mProfiles) {
            if (profile.getmFireBaseUUID().equals(firebaseId)) {
                return profile;
            }
        }
        return null;
    }


    private MatchmakingLab(Context context) {
        mProfiles = new ArrayList<>();
        uids = new ArrayList<>();
        //get async database snapshot
        getDataSnapshotOnce();
    }

    private void getDataSnapshotOnce() {
        //firebase setup, get db instance and current user
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabase = mFirebaseDatabase.getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        ValueEventListener getUnseenId = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snap : dataSnapshot.getChildren()) {

                    uids.add(snap.getValue().toString());
                }
                mDataSnapshot = dataSnapshot;
                createProfiles();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        mDatabase.child("users").child(mFirebaseUser.getUid()).child("mUnseenId").addListenerForSingleValueEvent(getUnseenId);
    }

    private void createProfiles() {
        for (String aUser : uids) {
            Profile profile = new Profile();
            profile.setmFireBaseUUID(aUser);

            try {
                //get prof info from firebase
                profile.pullFireBase();
            } catch (Exception e){
                //plow through
                continue;
            }
            mProfiles.add(profile);
        }
    }

}
